﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public float VerticalSpeed = 5f;
    public float HorizontalSpeed = 5f;
	
	void Update () {
        float zDistance = Input.GetAxis("Vertical") * VerticalSpeed * Time.deltaTime;
        float xDistance = Input.GetAxis("Horizontal") * HorizontalSpeed * Time.deltaTime;

        Vector3 newPos = new Vector3(transform.position.x + xDistance, transform.position.y, transform.position.z + zDistance);
        transform.position = newPos;
	}
}
