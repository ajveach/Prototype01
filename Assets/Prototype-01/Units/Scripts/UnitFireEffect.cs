﻿using UnityEngine;
using System.Collections;

public class UnitFireEffect : MonoBehaviour {
    ParticleSystem effect;

    void Awake()
    {
        effect = GetComponent<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update () {
        if (effect.isStopped)
            Destroy(this);
	}
}
