﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public class Unit : MonoBehaviour {
    private Path path;
    private int currentPathWaypoint;
    private Transform pathDestination;
    public float nextWaypointDistance = 3;
    private CharacterController CC;
    private Seeker seeker;
    public float MoveSpeed = 20;

    public enum Statuses
    {
        Positioning,
        Moving,
        Firing
    }
    private Statuses _currentStatus = Statuses.Positioning;
    public Statuses CurrentStatus {
        get
        {
            return _currentStatus;
        }
        set
        {
            _currentStatus = value;
            if (_currentStatus == Statuses.Firing)
                StartCoroutine("FireAtValidTargets");
            else
                StopCoroutine("FireAtValidTargets");
        }
    }

    public enum UnitTypes { Playable, Automated}
    public UnitTypes UnitType;


    /**
     * Firing Attributes
    **/
    public float FireDistance;
    public float FireDamage;
    public float FireRadius;
    public float FireRate;
    public Transform FireTarget;
    public GameObject FireEffect;

    void Awake()
    {
        CC = GetComponent<CharacterController>();
        seeker = GetComponent<Seeker>();
    }

    public void MoveToLookoutPoint(Transform destination)
    {
        pathDestination = destination;
        Vector3 flatDest = destination.position;
        flatDest.y = transform.position.y;
        seeker.StartPath(transform.position, flatDest, OnPathGenerated);
    }

    void OnPathGenerated(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentPathWaypoint = 0;
            CurrentStatus = Statuses.Moving;
        }
    }

    void MoveAlongPath()
    {
        if (currentPathWaypoint >= path.vectorPath.Count)
        {
            transform.position = pathDestination.position;
            transform.rotation = pathDestination.rotation;
            path = null;
            CurrentStatus = Statuses.Firing;
            return;
        }

        //Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentPathWaypoint] - transform.position).normalized;
        dir *= MoveSpeed * Time.fixedDeltaTime;
        CC.SimpleMove(dir);
        transform.rotation = Quaternion.LookRotation(dir);

        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (Vector3.Distance(transform.position, path.vectorPath[currentPathWaypoint]) < nextWaypointDistance)
        {
            currentPathWaypoint++;
            return;
        }
    }

    IEnumerator FireAtValidTargets()
    {
        while (true)
        {
            // If there is currently no target OR if the current target has gone out of range find another target
            if (FireTarget == null || Vector3.Distance(transform.position,FireTarget.position) > FireDistance)
                FindNextTarget();

            if (FireTarget != null)
            {
                // Fire logic
                Debug.Log("Firing at " + FireTarget.gameObject.name);
                Instantiate(FireEffect, transform.position, Quaternion.identity);
            }
            
            yield return new WaitForSeconds(1 / FireRate);
        }
    }

    void FindNextTarget()
    {
        // Unset any existing target
        FireTarget = null;

        GameObject[] enemies;
        if(gameObject.tag == "TeamA")
            enemies = GameObject.FindGameObjectsWithTag("TeamB");
        else
            enemies = GameObject.FindGameObjectsWithTag("TeamA");

        float closestDistance = FireDistance;
        // Loop through each enemy unit. If it's in range and closer than any previous units, make it the current target
        // TODO: Add raycasting to see if it's visibile to this unit
        foreach (GameObject enemy in enemies)
        {
            float distance = Vector3.Distance(transform.position, enemy.transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                FireTarget = enemy.transform;
            }
        }
    }

    void FixedUpdate()
    {
        if (CurrentStatus == Statuses.Moving)
            MoveAlongPath();
    }
}
