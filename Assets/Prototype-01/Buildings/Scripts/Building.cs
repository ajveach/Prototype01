﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Building : MonoBehaviour {
    List<LookoutPoint> LookoutPoints = new List<LookoutPoint>();

    Player player;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player").GetComponent<Player>();

        foreach (Transform child in transform)
        {
            LookoutPoint LP = child.gameObject.GetComponent<LookoutPoint>();
            LP.building = this;
            LookoutPoints.Add(LP);
        }
	}

    public void OnMouseEnter()
    {
        if(player.UnitAwaitingDestination != null)
            ShowLookoutPoints();
    }

    public void OnMouseExit()
    {
        HideLookoutPoints();
    }

    void ShowLookoutPoints()
    {
        foreach (LookoutPoint LP in LookoutPoints)
            LP.Show();
    }

    void HideLookoutPoints()
    {
        foreach (LookoutPoint LP in LookoutPoints)
            LP.Hide();
    }
}
