﻿using UnityEngine;
using System.Collections;

public class LookoutPoint : MonoBehaviour {
    Renderer rend;
    public Building building;

    void Start()
    {
        rend = renderer;
    }

    public void Show()
    {
        rend.enabled = true;
    }

    public void Hide()
    {
        rend.enabled = false;
    }

    void OnMouseEnter()
    {
        building.OnMouseEnter();
    }

    void OnMouseExit()
    {
        building.OnMouseExit();
    }

    void OnMouseDown()
    {
        Player player = GameObject.Find("Player").GetComponent<Player>();
        if (player.UnitAwaitingDestination != null)
        {
            player.UnitAwaitingDestination.GetComponent<Unit>().MoveToLookoutPoint(transform);
            player.UnitAwaitingDestination = null;
        }

    }
}
