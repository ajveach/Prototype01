﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Player : MonoBehaviour {
    string _name = "Player";
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }

    float _money = 0;
    public float Money
    {
        get
        {
            return _money;
        }
        set
        {
            _money = value;
            MoneyValue.text = Mathf.Floor(_money).ToString();
        }
    }

    private float MoneyPerSecond = 1;
    private Text MoneyValue;

    public GameObject UnitAwaitingDestination;
    private List<GameObject> Units = new List<GameObject>();

    public enum Teams { TeamA,TeamB}
    public Teams Team;

    void Start()
    {
        MoneyValue = GameObject.Find("Money Value").GetComponent<Text>();
    }

    void Update()
    {
        Money += MoneyPerSecond * Time.deltaTime;

        if (Input.GetButtonDown("Create Unit"))
            AddDemoUnit();
    }

    void AddDemoUnit()
    {
        GameObject newUnit = (GameObject)GameObject.Instantiate(Resources.Load("DemoUnit"), transform.position, Quaternion.identity);
        newUnit.tag = Team.ToString();
        Units.Add(newUnit);
        UnitAwaitingDestination = newUnit;
    }


}
